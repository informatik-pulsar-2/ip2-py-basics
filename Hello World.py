#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 14:30:08 2019

@author: anthy
"""

# Hello world
print("Hello world!")

# Input & Output
variable = input("Mach hier deine Eingabe >> ")
print("Deine Eingabe war", variable)

# Datentypen
ganzzahl = 9 # int()
decimalzahl = 9.0 # float()
text = "c" # str()
wahrheitswert = True # bool()
wahrheitswert_invertiert = False # bool()
liste = [9, 9.0, text, wahrheitswert,...] # list()

# Verzweigungen

if 2-1 == 0: # Wenn dies True ist..
    print("2-1 gleich 0") # dann das
elif 1-1 == 0: # oder wenn dies..
    print("1-1 gleich 0") # dann das
else: # oder, wenn nichts von davor..
    print("ansonsten") # dann das.

print("Ich werde immer ausgegeben")




# Beispiel

passwort = input("Was ist dein Passwort? >> ")

geheimes_passwort = "123456"

if passwort == geheimes_passwort:
    print("Du kommst rein")
else:
    print("Falsches Passwort!")




# Funktionen
    
# Funktionsdefinition
def QuadratischeFunktion(a, b, c, x):
    y = (a * x * x) + (b * x) + c
    
    return y

mein_a = int(input("Gib a ein: "))
mein_b = int(input("Gib b ein: "))
mein_c = int(input("Gib c ein: "))
mein_x = int(input("Gib x ein: "))

ergebnis= QuadratischeFunktion(mein_a, mein_b, mein_c, mein_x)

print("Das Ergbnis ist: ", ergebnis)


