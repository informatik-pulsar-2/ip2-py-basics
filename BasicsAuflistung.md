# Python-Basics

## Module

```python
import time # Importiert das Modul mit dem Namen "time"

start_time = time.time() # Ruft die Methode "time" des Moduls "time" auf
```

## Datentypen

```python
variable_1 = 123 # int()
variable_2 = 1.23 # float()
variable_3 = "abc" # str()
variable_4 = False or True # bool()

if variable_4:
    print("True")
elif variable_3 == "Abc":
    print("Abc")
else:
    print(None)

array_1 = [0, "a", False] # list()
array_2 = (1.0, "b", True) # tuple()
```

## Funktionen

```python
def FuncName(a, b, c, x): # Funktionsdefinition
    y = (a * pow(x, 2)) + (b * x) + c
    print(a, "*", str(x) + "^2 +", b, "*", x, "+", c, "=", y)
    return y # Gibt die Variable "y" zurück

quad_ergebnis = FuncName(1, 2, 3, 4) # Funktionsaufruf (der Wert der Variable "y" der Funktion wird hier in der Variable "quad_ergebnis" gespeichert)
print(quad_ergebnis)
```

## Klassen

```python
class ClassName: # Klassendefinition
    def __init__(self): # Definition einer Klassenspezifischen Methode (im Grunde eine Funktion)
        self.inner_variable = False
        print("Initializing..")
    
    def InnerFunc(self, inner_variable = None): # Das Argument "inner_variable" hat als Standardwert "None"
        if not type(inner_variable) == bool: # "not a == b" kann auch "a != b" geschrieben werden
            inner_variable = not self.inner_variable

        self.inner_variable = inner_variable

        return self.inner_variable
    
    def __str__(self):
        return str(self.inner_variable)

ClassInstance = ClassName() # Initialisiert die Klasse ClassName
print(ClassInstance)

iv = ClassInstance.InnerFunc()
print(iv, ClassInstance)

iv = ClassInstance.InnerFunc(False)
print(iv, ClassInstance)

array_3 = []
array_3.append(array_1)
```

## Eingabe

```python
input_1 = input("Input 1: ")
print(input_1)
```

## Loops

```python
for element in array_1:
    print(element)

for index in range(0, len(array_1)):
    element = array_1[index]
    print(index, element)

index_2 = 0
while index_2 < len(array_1):
    element = array_1[index_2]
    print(index_2, element)
    index_2 += 1
```

## Dateisystem

```python
file_handle = open("dateiname.dateiendung", "r") # Öffnet die Datei mit dem Namen "dateiname.dateiendung" im Lesemodus (ein sog. Dateihandler)
file_read = file_handle.readlines() # Liest den Inhalt der Datei in ein Array
file_handle.close() # Schließt den Dateihandler (gibt die Datei für andere Programme wieder frei)
print(file_read)
```
